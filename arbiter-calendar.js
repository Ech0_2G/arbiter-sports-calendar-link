Date.prototype.datestring = function() {
	var mm = (this.getMonth() + 1).toString();
	var dd = this.getDate().toString();
	var hh = this.getHours().toString();
	var min = this.getMinutes().toString();

	return [
		this.getFullYear(),
		mm.length === 2 ? '' : '0',
		mm,
		dd.length === 2 ? '' : '0',
		dd,
		'T',
		hh,
		min.length === 2 ? '' : '0',
		min,
		'00'
	].join('');
};

Date.from_arbiter_datestring = function(datestring) {
	//'8/2/2024 Fri 6:00 PM' Expected date string
	let [date, , time, ampm] = datestring.split(' ');
	let [month, day, year] = date.split('/');
	let [hour, minute] = time.split(':');
	hour = parseInt(hour);

	switch (ampm) {
		case 'PM':
			hour += 12
			break;
		default:
			break;
	}

	return new Date(year, month - 1, day, hour, minute, '00');
};

let pageTitle = document.querySelector('.captionContainer');

let gameDateContainer = document.querySelector('#ctl00_ContentHolder_pgeGameView_conGameView_dgGames_ctl02_lblDate');

let gameDate = Date.from_arbiter_datestring(gameDateContainer.innerHTML);
let endDate = new Date(gameDate);
endDate.setHours(endDate.getHours() + 3);

let gameDateString = `${gameDate.datestring()}/${endDate.datestring()}`;

let gameName = document.querySelector('#ctl00_ContentHolder_pgeGameView_conGameView_dgGames_ctl02_lblSportLevel').innerHTML;
let gameLocation = document.querySelector('#ctl00_ContentHolder_pgeGameView_conGameView_dgGames_ctl02_lnkSite').innerHTML.split(", ")[0];

let calendarLink = document.createElement("a");

calendarLink.href = `http://www.google.com/calendar/render?action=TEMPLATE&text=${gameName} Referee&dates=${gameDateString}&location=${gameLocation}, TN&trp=false`;
calendarLink.target = "_blank";
calendarLink.ref = "nofollow";
calendarLink.textContent = "Add to Calendar"

pageTitle.append(calendarLink);
